import asyncio


class Server:
    message_len = 4096

    def __init__(self):
        self.queue = asyncio.Queue()  #[pid, msg]-[pid, msg]-[pid, msg]...
        self.players = {}
        _send_back = asyncio.ensure_future(self.send_back())

    async def send_back(self):
        while True:
            send_id, message = await self.queue.get()
            for pid in self.players:
                if pid == send_id:
                    continue
                else:
                    self.players[pid].write(message)
                    await self.players[pid].drain()


    async def receive_message(self, reader, writer):
        pid = await reader.read(16)
        self.players[pid] = writer
        while True:
            message = await reader.read(self.message_len)
            if message:
                await self.queue.put((pid, message))
            else:
                writer.close()
                self.players.pop(pid)
                return None


def main():
    s = Server()
    loop = asyncio.get_event_loop()
    coro = asyncio.start_server(s.receive_message, '127.0.0.2', 9999)
    server = loop.run_until_complete(coro)

    try:
        loop.run_forever()

    except KeyboardInterrupt:
        pass

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()


if __name__ == '__main__':
    main()
