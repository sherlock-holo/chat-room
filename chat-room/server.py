import argparse
import asyncio
import json
import time
import websockets


class Server:
    def __init__(self):
        self.queue = asyncio.Queue()
        self.players = {}
        notify_all = asyncio.ensure_future(self.notify_all_handle())

    async def notify_all_handle(self):
        while True:
            send_id, message, send_time = await self.queue.get()
            for pid in self.players:
                try:
                    if pid == send_id:
                        continue

                    data = {'nick': self.players[pid]['nick'],
                            'message': self.players[pid]['message'],
                            'send_time': send_time}
                    data = json.dumps(data)
                    await self.players[pid]['transport'].send(data)

                except websockets.ConnectionClosed:
                    pass

    async def handle(self, websocket, path):
        data = await websocket.recv()
        data = json.loads(data)
        pid = data['pid']
        nick = data['nick']

        self.players[pid] = {'nick': nick, 'transport': websocket}

        while True:
            try:
                data = await websocket.recv()
                data = json.loads(data)
                message = data['message']
                send_time = time.time()
                await self.queue.put((pid, message, send_time))

            except websockets.ConnectionClosed:
                self.players.pop(pid)
                websocket.close()
                return None


def main():
    parser = argparse.ArgumentParser(description='chat room server')
    parser.add_argument('-h', '--host', help='listen host')
    parser.add_argument('-p', '--port', help='listen port')

    args = parser.parse_args()

    if args.host:
        host = args.host
    else:
        host = ''

    if args.port:
        port = args.port
    else:
        port = 911

    serve = Server()
    loop = asyncio.get_event_loop()
    coro = websockets.serve(serve.handle, host, port)
    server = loop.run_until_complete(coro)

    try:
        loop.run_forever()

    except KeyboardInterrupt:
        pass

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()


if __name__ == '__main__':
    main()
